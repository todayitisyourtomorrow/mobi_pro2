import {Button, Nav, NavDropdown, Container, Form, Navbar} from 'react-bootstrap';
import categories from "./categories";
function Navigator(props) {
    const products = () => {
        props.setName(prev => 'discounted')
    }
    const  onclickhome = () => {
        props.setName(prev => 'home')
    }

    const searchButton = () => {
        props.setBut(props.search)
    }


    return(
        <>
            <Navbar bg="dark" expand="lg" variant="dark">
                <Container fluid>
                    <Navbar.Brand href="#">Navbar scroll</Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse id="navbarScroll">
                        <Nav
                            className="me-auto my-2 my-lg-0"
                            style={{ maxHeight: '100px' }}
                            navbarScroll
                        >
                            <Nav.Link onClick={onclickhome} href="#">Домой</Nav.Link>
                            <Nav.Link onClick={products}>Скидки</Nav.Link>
                            <NavDropdown title="Категории" id="navbarScrollingDropdown">

                                {categories.map((prod =>  <NavDropdown.Item onClick={() => props.setName(prev => prod.id)}>{prod.short_title}</NavDropdown.Item>))}
                            </NavDropdown>
                        </Nav>
                        <Form className="d-flex">
                            <Form.Control
                                type="search"
                                placeholder="Search"
                                className="me-2"
                                aria-label="Search"
                                onChange={(event) => {props.setSearch(event.target.value)}}
                            />
                            <Button variant="outline-success" onClick={searchButton} >Search</Button>
                        </Form>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

        </>
    )
}
export default Navigator