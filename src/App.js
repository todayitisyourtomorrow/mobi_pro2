import Addfile from './addFile'
import Navigator from "./naV";
import './App.css'
import React, {useState} from "react";

function App() {
    const [name, setName] = useState('home')
    const [search, setSearch] = useState('')
    const [but, setBut] = useState('')
  console.log(but)
    return (
        <>
            <Navigator setName={setName} setSearch={setSearch} search={search} setBut={setBut}/>
            <Addfile name={name} but={but}/>
        </>
    )
}

export default App