import data from './products'
import {Container, Row, Col, Card, Button} from 'react-bootstrap';

function Addfile(props) {
    const products = props.name === 'discounted' ? data.filter(prev => prev.discount && prev.title.toLowerCase().startsWith(props.but.toLowerCase())
    ) : props.name === 'home' ? data.filter(item => item.title.toLowerCase().startsWith(props.but.toLowerCase())
    ) : data.filter(item => item.category_id === props.name && item.title.toLowerCase().startsWith(props.but.toLowerCase()))

  // const filteredProducts = filterProducts.filter((product) =>
  //   product.name.toLowerCase().includes(searchText.toLowerCase())
  // );s




    return(
        <>
            <Container>
                <Row>
                    {products.map((item) => {
                        return(
                            <Col className={'my-3'}>
                                    <Card className={'Card'} >
                                        {item.discount? <div className={'div'}>{item.discount} % </div>
                                            :null}
                                        <div style={{height:'250px', width:'280px'}}>
                                            <div className={'divImg'} style={{backgroundImage: `url(${item.main_image.path.original})`}} />
                                        </div>
                                        <Card.Body style={{display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
                                            <Card.Title>{item.title}</Card.Title>

                                            <Button variant="primary" className={'CardButton'}>
                                                <span>{item.price * (1- item.discount / 100 )} сом</span>
                                                {item.discount ?
                                                  <>
                                                    <span style={{textDecoration: "line-through", color:'red', fontSize:'12px'}}> {item.price}</span>
                                                    </>
                                                    :null}
                                            </Button>
                                        </Card.Body>
                                    </Card>
                                </Col>
                        )
                    })}
                </Row>
            </Container>
        </>
    )
}
export default Addfile